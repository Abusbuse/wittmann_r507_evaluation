from calcul_volume_sphere import calculer_volume_sphere
import pytest

def test_calculer_volume_sphere():
    assert calculer_volume_sphere(3) == pytest.approx(113.097335, rel=1e-6)
    assert calculer_volume_sphere(0) == 0
    with pytest.raises(ValueError):
        calculer_volume_sphere(-2)
