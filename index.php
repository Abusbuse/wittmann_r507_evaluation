<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultat du Calcul</title>
</head>
<body>
    <?php
        // Lire le contenu du fichier texte
        $contenu = file_get_contents('volume.txt');
        
        // Afficher le contenu du fichier texte
        echo '<p>Le volume du cône est de ' . $contenu . ' cm<sup>3</sup>.</p>';
    ?>
</body>
</html>