"""
Module calcul_volume_sphere
Ce module contient des fonctions pour calculer le volume d'une sphère.
"""

import math

def calculer_volume_sphere(rayon):
    """
    Calcule le volume d'une sphère en fonction du rayon.
    
    Parameters:
    - rayon (float): Le rayon de la sphère.
    
    Returns:
    - float: Le volume de la sphère.
    """
    if rayon < 0:
        raise ValueError("Le rayon ne peut pas être négatif.")

    volume = (4 * math.pi * rayon ** 3) / 3
    return volume

def enregistrer_resultat_fichier(volume, fichier):
    """
    Enregistre le volume dans un fichier texte.

    Parameters:
    - volume (float): Le volume de la sphère.
    - fichier (str): Le nom du fichier où enregistrer le volume.
    """
    with open(fichier, 'w', encoding='utf-8') as file:
        file.write(f"{volume}\n")

def main():
    """
    Fonction principale du script.
    Demande à l'utilisateur d'entrer le rayon de la sphère
    et enregistre le volume dans un fichier.
    """
    try:
        rayon = float(input("Rayon de la sphère en cm : "))
    except ValueError:
        print("Veuillez entrer un nombre valide pour le rayon.")
        return

    volume = calculer_volume_sphere(rayon)
    enregistrer_resultat_fichier(volume, "volume.txt")

if __name__ == '__main__':
    main()
