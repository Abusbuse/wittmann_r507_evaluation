# Evaluation R507: Automatisation de la chaîne de production

Auteur: Wittmann Gregory

## présentation du projet

Ce projet contient:

- un scrypt python qui permet le calcul de volume d'une sphère. L'utilisateur va pouvoir rentrer le rayon et le scrypt va calculer le volume de la sphère. Ce même fichier va enregistrer le résultat dans un fichier texte.
- un fichier de test unitaire pour le scrypt python afin de vérifier si le rayon n'est pas un nombre négatif ou null.
- un fichier index.php permettant l'affichage du résultat du scrypt python de mainière très simple avec juste le résultat du volume de la sphère.
- un fichier Dockerfile permettant de créer une image docker contenant une image php et qui va lancer le téléchargement des fichiers requis pour le bon fonctionnement du site web.
- un fichier .gitlab-ci.yml, étant le fichier principal de ce projet, qui avoir 3 étapes:
  - lint: qui va lancer le linter pour le scrypt python afin de vérifier si le code est bien écrit
  - test: qui va lancer le test unitaire
  - build: qui va créer l'image docker

Lien vers le gitlab du projet: <https://gitlab.com/Abusbuse/wittmann_r507_evaluation>
