# Utilisez une image de base PHP avec Apache
FROM php:7.4

COPY . /app

WORKDIR /app

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip
# Exposez le port 80 (par défaut pour Apache)
EXPOSE 9090

# CMD pour démarrer Apache
CMD ["php", "-S", "0.0.0.0:9090"]
